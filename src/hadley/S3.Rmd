% R 進階探討 -- Hadley's R 電子書
% S3
% Taiwan R User Group

```{r library, include=FALSE}
# outline <- list(
# 	"基礎知識" = list(
# 		"什麼是environment" = c("environment是一個reference"), 
# 		"查詢和修改environment" = NULL, 
# 		"特殊的environment" = c("globalenv", "baseenv", "emptyenv"),
# 		"查詢物件的所屬environment" = NULL),
# 	"函數的environment" = c("創造函數時的environment", "函數運作時的environment", "每次運作時environment都會重製", "使用函數的environment"),
# 	"Explicit scoping with local" = NULL,
# 	"賦值(Assignment): 將一個名稱指定給一個變數(Binding)" = c("Regular Binding", "Constant", "<<-", "Delay Binding", "Active Binding"))
source("../gist/outline/outline.R")
outline.index <- 0
```

# S3

- `S3`使用的是"generic-function 物件導向"，和常用語言的物件導向不相同。
    - 一般物件導向語言是呼叫一個物件的方法，由物件來動態決定實際上是哪一個函數執行
    - `S3`是由一種稱做**generic function**的函數來決定實際上是哪一個函數執行
    - `base`